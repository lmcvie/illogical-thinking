this.bar=null;
this.preloadBar = null;

this.ready = false;
Preloader =
{

    /**
     * sets up the loading bar and draws it
     * loads all the assets for the game.
     */
    preload: function()
    {
        this.bar = this.add.sprite(450, 300, "PreloaderBar");
        this.preloadBar = this.add.sprite(450, 300, 'PreloaderBarFull');
        this.load.setPreloadSprite(this.preloadBar,0);
        loadAssets();

    },


    /**
     * plays the music for the game.
     */
    create: function()
    {
        music = game.add.audio("music",1,true);
        music.volume =5;
        music.play('',0,1,true);
        this.preloadBar.cropEnabled = false;

    },


    /**
     * checks to see if the music is loaded, if it is it loads the main menu.
     */
    update: function()
    {
           this.ready=false;
        if (this.cache.isSoundDecoded('music') && this.ready == false)
        {
            this.ready = true;
            this.state.start('menu');
        }

    }
}

