
var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, 'AccidentProne');

//adds the different game states and says which one to start with
game.state.add('menu',Menu);
game.state.add("highScores", Highscores);
game.state.add("game", Game);
game.state.add("Boot", BasicGame.Boot);
game.state.add("Preloader", Preloader);
game.state.add("EndGame", EndGame);
game.state.start("Boot");
