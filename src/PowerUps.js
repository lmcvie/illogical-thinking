
var trampolineA,
    trampolineB,
    steelSpike;

/**
 * Draws the first set of power ups.
 */
function drawFirstPowerUps()
{
    trampolineA = drawSprite("TrampolineA", 4000, 2800);
    trampolineB = drawSprite("TrampolineB", 6000, 2800);
    steelSpike = drawSprite("Steelspike", 5000, 2800);
    trampolineA.name = "TrampolineA";
    trampolineB.name = "TrampolineB";
    steelSpike.name = "Steelspike";
}

/**
 * Returns a random number for the distance of a power up from the characters current position.
 * @returns {*} A random number for the distance of a power up.
 */
function RandomXValue()
{
    return game.rnd.integerInRange(500, 1500);
}

/**
 * Checks if the conditions are correct for drawing another power up and if it is then it draws the next power up
 * at the players x pos + a random value between 500 and 1500.
 * @param {number} playerXPosition is the current x position of the player when the next power up is going to be drawn. It is draw at a random value between 500 and 1500 from this point
 */
function DrawNextPowerUp(playerXPosition)
{
 //random int value that will be added on to previous value
     var randomXValue = RandomXValue();

     if((playerXPosition - 950) > trampolineB.position.x)
     {
        trampolineB.body.reset((playerXPosition+randomXValue), 2800);
        trampolineB.position.x =  +randomXValue;


     }
        // if power up is spike
     if((playerXPosition - 950)> steelSpike.position.x)
     {
         steelSpike.body.reset((playerXPosition+randomXValue), 2800);
         steelSpike.position.x = (playerXPosition+randomXValue);
     }
        // if power up is trampolineA
     if((playerXPosition - 950)> trampolineA.position.x)
     {
        trampolineA.body.reset((playerXPosition+randomXValue), 2800);
        trampolineA.position.x = (playerXPosition+randomXValue);
     }




}