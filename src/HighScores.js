
var back, music, onScreen;

Highscores =
{
    preload: function()
    {


    },

    /**
     * adds assets required for the high scores screen
     */
    create: function()
    {
        // add audio
        music = game.add.audio("ButtonPress");
        music.volume = 10;
        onScreen = true;

        // Display a sprite on the screen
        // Parameters: name of the sprite, x position, y position
        drawSprite("Sky", 0, 0,800,600);
        drawSprite("Title", 660, 25,800,600);
        drawSprite("ScoreTile", 700, 100,800,600);
        drawSprite("ScoreTile", 700, 170,800,600);
        drawSprite("ScoreTile", 700, 240,800,600);
        drawSprite("ScoreTile", 700, 310,800,600);
        drawSprite("ScoreTile", 700, 380,800,600);
        drawSprite("EarthMenu", 400, 550,800,600);
        drawSprite("EarthMenu", 697, 550,800,600);
        drawSprite("RagdollScores",920,300,800,600);
        back = drawSprite("Back", 0,0,800,600);
        getTable();


    },


    /**
     *  sets up back button for input
     */
    update: function()
    {
        setUp(back);
        inputCheck(back,switchMainMenu);
    }
}


/**
 * switches main menu when back is pressed and plays button sound
 */
function switchMainMenu () {
    ButtonPress.play();
    onScreen = false;
    this.game.state.start('menu');
    text=game.debug.text("");
}


/**
 *
 * @param {object} obj is the data from the online high scores, it length loops through the object storing the whole data in the variable s, it then displays the top 5 scores onto the high score screen
 */
function showScoreTable(obj) {
    var s = "", i;
        for (i = 0; i < obj.scores.length; i += 1)
        {
             s += obj.scores[i].name + ' : ' + obj.scores[i].score + " ";
             if (onScreen == true && i<5)
             {
               text=game.debug.text(obj.scores[i].name + ' : ' + obj.scores[i].score + " ", 720,140+(70*i));
             }

            }
    console.log(s);

}
/**
 * retrieves high score table from url then wehen it recieves the data does a call back to show scores table passing in the data object it is given from the online data storage
 */
function getTable() {
    var uri = "https://mcm-highscores-hrd.appspot.com/";
    var url = uri + "scoresjsonp?game=accident-prone&func=?";
    $.ajax({
        type: "GET",
        url: url,
        async: true,
        // Note, instead of this we could have a success function...
        jsonpCallback: 'showScoreTable',
        contentType: 'application/json',
        dataType: 'jsonp',
        error: function (e) {
            window.alert(e.message);
        }

    });
}