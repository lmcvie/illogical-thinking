
var gameButton, highScoresButton, ButtonPress, music;
Menu =
{

    preload: function()
    {

    },

    /**
     * Adds assets required for the menu and draws the menus.
     */
    create: function()
    {

       //add audio

        game.debug.text("");
        ButtonPress = game.add.audio("ButtonPress");
        ButtonPress.volume = 10;
        DrawMenu();
        gameButton.string = "newButton";
        gameButton.anchor.set(0.0);

        highScoresButton.string = "highScoresButton";
        highScoresButton.anchor.set(0.0);


},


    /**
     * sets up the buttons for input and then checks for input.
     */
    update: function()
    {
        setUp(highScoresButton);
        setUp(gameButton);


        inputCheck(highScoresButton, switchHighScores);
        inputCheck(gameButton, switchNewGame);
    }

}

/**
 * switch to high scores on button click
 */
function switchHighScores () {
   ButtonPress.play();
   this.game.state.start('highScores');
}

/**
 * switch to game on button click
 */
function switchNewGame(){
    ButtonPress.play();
    this.game.state.start('game');
}

/**
 * Draws the menu
 */
function DrawMenu()
{
    // Display a sprite on the screen
    // Parameters: name of the sprite, x position, y position
    drawSprite("Sky", 0, 0, 1000, 1000);
    drawSprite("Title", 575, 50,80,60);

    drawSprite("RagdollMenu", 590, 285, 800, 600);
    drawSprite("EarthMenu", 300, 550,  800, 600);
    drawSprite("EarthMenu", 697, 550, 800, 600);
    drawSprite("EarthMenu", 0, 550, 800, 600);
    drawSprite("EarthMenu", 900, 550, 800 ,600);
    drawSprite("EarthMenu", 1200, 550, 800 ,600);
    drawSprite("sun",1400,50,800,600);
    highScoresButton = drawSprite("HighScoresButton",600, 225,  800, 600);

    gameButton = drawSprite("NewButton", 600, 125, 800, 600);
}





