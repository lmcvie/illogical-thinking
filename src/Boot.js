BasicGame = {

    orientated: false

};

BasicGame.Boot = function (game) {
};


BasicGame.Boot.prototype = {

    /**
     * loading of the loading bar is done here as it is done while the game
     * is being booted.
     */
    preload: function () {
        //gets loaded here so will display as other assets are loaded
        loadSprite("PreloaderBar", "assets/loading_bar.png");
        loadSprite("PreloaderBarFull","assets/loading.png");


    },

/**
 * Sets up the screen, only allowing one pointer or finger, checks
 * which game device is being used and sets the screen up accordingly
 * Moves onto the preloader state, where the rest of the assets are loaded.
 */
    create: function () {

        game.input.maxPointers = 1;
        game.stage.disableVisibilityChange = true;
        console.log(this.game.device.desktop);
        if (this.game.device.desktop)
        {
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.minWidth = 480;
            game.scale.minHeight = 260;
            game.scale.maxWidth = 1024;
            game.scale.maxHeight = 768;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            game.scale.setScreenSize(true);
            console.log("desktop");
        }
        else
        {
            console.log("mobile");
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.minWidth = 480;
            game.scale.minHeight = 260;
            game.scale.maxWidth = 1024;
            game.scale.maxHeight = 768;
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            game.scale.forceOrientation(true, false);
            game.scale.setScreenSize(true);
        }

        this.state.start('Preloader');

    }


};