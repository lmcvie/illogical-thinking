var MoveUp, MoveDown, Select, Submit;

EndGame =
{

    preload: function()
    {
        PlayerName="";
    },

    /**
     * creates all the assets needed for the end game screen
     */
    create: function()
    {
        drawSprite("Sky", 0, 0, 1000, 1000);
        drawSprite("tileground", 0, 600);
        drawSprite("Box", 500,400);
        drawSprite("Box", 700,400);
        drawSprite("Box", 900,400);
        drawSprite("Box", 800,200);
        drawSprite("Box", 600,200);
        drawSprite("sun", 100,100);

        MoveUp = drawSprite("MoveUp", 950, 490);
        MoveDown = drawSprite("MoveDown",550, 490);
        Select = drawSprite("Select", 750, 490);
        Submit = drawSprite("SubmitButton",1350,614);



    },


    /**
     * displays the text for the player to read and the name as it gets added to along with the score and sets up buttons and checks them for being pressed
     */
    update: function()
    {
        setUp(MoveUp);
        setUp(MoveDown);
        setUp(Select);
        setUp(Submit);
        inputCheck(Select,StoreChar);
        inputCheck(MoveUp,moveLetterUp);
        inputCheck(MoveDown,moveLetterDown);
        inputCheck(Submit,submitHighScore);
        game.debug.text("Character", 650,250 );
        game.debug.text("Selection", 650,265 );
        game.debug.text(chr, 690,320 );
        game.debug.text("Name: ",850,250);
        if(PlayerName != undefined) game.debug.text(PlayerName,870,320);
        game.debug.text("Well Done you got a score of: " + score,500,100);
        game.debug.text("Please enter your name. To enter your name please use move up and move down to scroll through the letters", 270,150);
        game.debug.text("and select to choose the letter, that letter will then be added to your name press submit to finish entering your name",270,165);
    }
}

/**
 * call to submit score and after submitting score successfully will go back to the menu
 */
function submitHighScore()
{
    submitScore("accident-prone",PlayerName,"noEmailNeeded@test.com",Math.floor(score));

    first=true;
    chr = String.fromCharCode(65);
    score = 0;
    this.game.state.start('menu');

}



/**
 *
 * @param {string} gameName name of the gamename on the online data store
 * @param {string} name name of the player who has the score
 * @param {string} email email of the player(unnecessary in the game but is a required param online so will be set to a default email)
 * @param {int} score score the player got during the game
 */
function submitScore(gameName,name,email,score) {
    var uri = "https://mcm-highscores-hrd.appspot.com/";
    var url = uri + "score?game={0}&nickname={1}&email={2}&score={3}&func=?";
    url = url.replace('{0}', gameName);
    url = url.replace('{1}', name);
    url = url.replace('{2}', email);
    url = url.replace('{3}', score);
    console.log(url);
    $.ajax({
        type:  "GET",
        url:   url,
        async: true,
        contentType: 'application/json',
        dataType: 'jsonp',
        success: function (json) {
            console.log(json.result);
        },
        error: function (e) {
            window.alert(e.message);
        }
    });
}

