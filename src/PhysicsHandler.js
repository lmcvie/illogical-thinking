var RagdollSmile,
    RagdollTorso,
    RagdollRightArm,
    RagdollLeftArm,
    RagdollRightForearm,
    RagdollLeftForearm,
    RagdollWaist,
    RagdollLeftThigh,
    RagdollLeftShin,
    RagdollRightThigh,
    RagdollRightShin,
    torsoToSkateboard,
    leftShinsToSkateboard,
    rightShinsToSkateboard,
    leftThighsToSkateboard,
    rightThighsToSkateboard,
    leftKneeJoint,
    rightKneeJoint,
    launched,
    hasHitGround;

/**
 * Sets up the worlds gravity and enables physics on all the objects that require physics.
 * calls other functions also which set up properties within the world.
 */
function startPhysicsSystem()
{
    launched = false;
    game.physics.startSystem(Phaser.Physics.P2JS); // apply P2 physics
    game.physics.p2.gravity.y = 800;
    game.physics.p2.gravity.x = 200;
    //now enable objects to be used with P2 physics
    game.physics.p2.enable([
        RagdollSmile,
        RagdollTorso,
        RagdollRightArm,
        RagdollLeftArm,
        RagdollRightForearm,
        RagdollLeftForearm,
        RagdollWaist,
        RagdollLeftThigh,
        RagdollLeftShin,
        RagdollRightThigh,
        RagdollRightShin,
        ground,
        ramp,
        Skateboard,
        rock,
        tileGround,
        trampolineA,
        trampolineB,
        steelSpike],
        false);


    InitialiseProperties(); //initialise object bodies properties
    LockConstraints(); // lock constraint objects together
    AddingAccurateCollisionShapes(); // apply accurate collision masks to objects (some from JSON file
    JointSetUp(); // set up the joints for the ragdoll


}


/**
 * This function is used to check if a collision has occurred between two objects. If a collision has occurred then
 * it deals with it appropriately.
 * @param {object} target is the object that is being touched by the object that this method is called upon.
 *
 */
function CollisionCheck(target)
{

    var bounceSound = game.add.audio("Bounce");
    var spikeSound = game.add.audio("Spike");

    if(target.sprite.name == "tileGround")
    {
        RagdollTorso.body.velocity.x = RagdollTorso.body.velocity.x - ((RagdollTorso.body.velocity.x)*(10/100));
        RagdollTorso.body.velocity.y = (50*RagdollTorso.body.velocity.y);
        hasHitGround+=1;
        hitGround = true;
    }

    if(target.sprite.name == "TrampolineA")
    {
        RagdollTorso.body.velocity.y = (75*RagdollTorso.body.velocity.y);
        RagdollTorso.body.velocity.x = (2*RagdollTorso.body.velocity.x);
        bounceSound.play();
        console.log("trampolineA collision");
    }

    if(target.sprite.name == "TrampolineB")
    {
        RagdollTorso.body.velocity.y = (75*RagdollTorso.body.velocity.y);
        RagdollTorso.body.velocity.x = (2*RagdollTorso.body.velocity.x);
        bounceSound.play();
        console.log("trampolineB collision");
    }

    if(target.sprite.name == "Steelspike")
    {
        RagdollTorso.body.velocity.y = 0;
        RagdollTorso.body.velocity.x = 0;
        spikeSound.play();
        console.log("spike collision");
    }

    if(RagdollTorso.body.velocity.y >400) RagdollTorso.body.velocity.y = 400;
}

/**
 * checks if the skateboard has collided with the rock, if it has then launch the player, play audio and break legs off.
 * @param {object} target is the object that is being touched by the object that this method is called upon.
 */
function checkLaunch(target)
{
    var boneBreak = game.add.audio("BoneBreak"); // play the bone breaking sound

    if(target.sprite.name == "Rock")
    {
        RagdollTorso.body.velocity.y= (50*Skateboard.body.velocity.y); // fire the ragdoll into the air after colliding depending on the

        // velocity achieved from tapping
        boneBreak.volume = 10;
        boneBreak.play();
        //remove constraints to skateboard as he is now flying through the air
        torsoToSkateboard = game.physics.p2.removeConstraint(torsoToSkateboard);
        leftThighsToSkateboard = game.physics.p2.removeConstraint(leftThighsToSkateboard);
        rightThighsToSkateboard = game.physics.p2.removeConstraint(rightThighsToSkateboard);
        leftKneeJoint = game.physics.p2.removeConstraint(leftKneeJoint);
        rightKneeJoint = game.physics.p2.removeConstraint(rightKneeJoint);
        Skateboard.body.static = true;
        launched = true;
    }
}


/**
 * This function locks the ragdoll to the skateboard.
 */
function LockConstraints()
{
    // lock ragdoll to skateboard whilst going down ramp
    torsoToSkateboard = game.physics.p2.createLockConstraint(Skateboard, RagdollTorso, [0, 150], 0);
    leftShinsToSkateboard = game.physics.p2.createLockConstraint(Skateboard, RagdollLeftShin, [30, 20], 0);
    rightShinsToSkateboard = game.physics.p2.createLockConstraint(Skateboard, RagdollRightShin, [-30, 20], 0);
    leftThighsToSkateboard = game.physics.p2.createLockConstraint(Skateboard, RagdollLeftThigh, [20, 60], 0);
    rightThighsToSkateboard = game.physics.p2.createLockConstraint(Skateboard, RagdollRightThigh, [-20, 60], 0);
}

/**
 *Clears the shapes that are automatically generated by P2 when physics is enabled on an object.
 *Accurate collision boxes are then loaded in, in JSON file format as that is what Phaser accepts.
 *PhysicsEditor is the program used to generate the data and file for the collision polygons.
 */
function AddingAccurateCollisionShapes()
{
    RagdollSmile.body.clearShapes();
    RagdollSmile.body.loadPolygon("RagdollCollision", "RagdollSmile");
    RagdollTorso.body.clearShapes();
    RagdollTorso.body.loadPolygon("RagdollCollision", "RagdollTorso");
    RagdollRightArm.body.clearShapes();
    RagdollRightArm.body.loadPolygon("RagdollCollision", "RagdollArm");
    RagdollLeftArm.body.clearShapes();
    RagdollLeftArm.body.loadPolygon("RagdollCollision", "RagdollArm");
    RagdollRightForearm.body.clearShapes();
    RagdollRightForearm.body.loadPolygon("RagdollCollision", "RagdollRightForearm");
    RagdollLeftForearm.body.clearShapes();
    RagdollLeftForearm.body.loadPolygon("RagdollCollision", "RagdollLeftForearm");
    RagdollWaist.body.clearShapes();
    RagdollWaist.body.loadPolygon("RagdollCollision", "RagdollWaist");
    RagdollLeftThigh.body.clearShapes();
    RagdollLeftThigh.body.loadPolygon("RagdollCollision", "RagdollThigh");
    RagdollLeftShin.body.clearShapes();
    RagdollLeftShin.body.loadPolygon("RagdollCollision", "RagdollShin");
    RagdollRightThigh.body.clearShapes();
    RagdollRightThigh.body.loadPolygon("RagdollCollision", "RagdollThigh");
    RagdollRightShin.body.clearShapes();
    RagdollRightShin.body.loadPolygon("RagdollCollision", "RagdollShin");
    rock.body.clearShapes();
    rock.body.loadPolygon("physicsData", "rock");
    Skateboard.body.clearShapes();
    Skateboard.body.loadPolygon("physicsData", "skateboard");
    ramp.body.clearShapes();
    ramp.body.loadPolygon("physicsData", "earth");
    trampolineA.body.clearShapes();
    trampolineA.body.loadPolygon("physicsData", "Trampoline");
    trampolineB.body.clearShapes();
    trampolineB.body.loadPolygon("physicsData", "Trampoline");
    steelSpike.body.clearShapes();
    steelSpike.body.loadPolygon("physicsData", "Steelspike");
}

/**
 * Sets up all the revolute constraints for the ragdoll. They are all done at what would be considered
 * joints on the human body. Also sets up the limits for the constraints.
 */
function JointSetUp()
{
    //Neck Joint Constraints
    var neckJoint = game.physics.p2.createRevoluteConstraint(RagdollSmile, [0,75],
        RagdollTorso, [0,1]);
    neckJoint.upperLimitEnabled = true;
    neckJoint.upperLimit = Math.PI / 32;
    neckJoint.lowerLimitEnabled = true;
    neckJoint.lowerLimit = -Math.PI / 32;

    //Knee Joint Constraints
    leftKneeJoint = game.physics.p2.createRevoluteConstraint(  RagdollLeftShin,  [0, -58],
        RagdollLeftThigh,  [0,-20]);
    rightKneeJoint= game.physics.p2.createRevoluteConstraint( RagdollRightShin , [0, -58],
        RagdollRightThigh, [0,-20]);
    leftKneeJoint .upperLimitEnabled = rightKneeJoint.upperLimitEnabled = true;
    leftKneeJoint.upperLimit = rightKneeJoint.upperLimit = Math.PI / 16;
    leftKneeJoint.lowerLimitEnabled = rightKneeJoint.lowerLimitEnabled = true;
    leftKneeJoint.lowerLimit = rightKneeJoint.lowerLimit = -Math.PI / 16;

    //Hip Joint Constraints
    var leftHipJoint = game.physics.p2.createRevoluteConstraint(  RagdollLeftThigh,  [30, 82],
        RagdollWaist,        [0,120]);
    var rightHipJoint = game.physics.p2.createRevoluteConstraint(  RagdollRightThigh, [-30, 82],
        RagdollWaist,        [0, 120]);
    leftHipJoint .upperLimitEnabled = rightHipJoint.upperLimitEnabled =  true;
    leftHipJoint.upperLimit =         rightHipJoint.upperLimit =         Math.PI / 32;
    leftHipJoint.lowerLimitEnabled =  rightHipJoint.lowerLimitEnabled =  true;
    leftHipJoint.lowerLimit =         rightHipJoint.lowerLimit =        -Math.PI / 32;

    //Spine Joint Constraint
    var spineJoint = game.physics.p2.createRevoluteConstraint(RagdollWaist,    [0, 10],
        RagdollTorso, [0, 65]);
    spineJoint.upperLimitEnabled = true;
    spineJoint.upperLimit = Math.PI / 100;
    spineJoint.lowerLimitEnabled = true;
    spineJoint.lowerLimit = -Math.PI / 100;

    // Elbow Joint Constraints
    var leftElbowJoint = game.physics.p2.createRevoluteConstraint(RagdollLeftForearm,  [70, 0],
        RagdollLeftArm,  [25,0]);
    var rightElbowJoint= game.physics.p2.createRevoluteConstraint(RagdollRightForearm, [-10, 0],
        RagdollRightArm, [20,0]);
    leftElbowJoint .upperLimitEnabled = rightElbowJoint.upperLimitEnabled =  true;
    leftElbowJoint.upperLimit =         rightElbowJoint.upperLimit =         Math.PI / 32;
    leftElbowJoint.lowerLimitEnabled =  rightElbowJoint.lowerLimitEnabled =  true;
    leftElbowJoint.lowerLimit =         rightElbowJoint.lowerLimit =        -Math.PI / 32;

    //Shoulder Joint Constraints
    var leftShoulder = game.physics.p2.createRevoluteConstraint(  RagdollTorso,     [-45, -10],
        RagdollLeftArm,  [-10,0]);
    var rightShoulder= game.physics.p2.createRevoluteConstraint(  RagdollTorso,     [40,  -10],
        RagdollRightArm, [10,0]);
    leftShoulder.upperLimitEnabled =  rightShoulder.upperLimitEnabled =  true;
    leftShoulder.lowerLimitEnabled =  rightShoulder.lowerLimitEnabled =  true;
    leftShoulder.upperLimit =         Math.PI / 2;
    rightShoulder.upperLimit =        Math.PI / 2;
    leftShoulder.lowerLimit =         -Math.PI / 2;
    rightShoulder.lowerLimit =        -Math.PI /2;
}

/**
 * This function is used to set up all the starting values for the objects with physics acting on them.
 */
function InitialiseProperties()
{
    trampolineA.body.kinematic = true;
    trampolineB.body.kinematic = true;
    steelSpike.body.kinematic = true;
    ground.body.static = true;
    tileGround.body.static = true;

    RagdollTorso.body.mass = 10;
    Skateboard.body.mass = 6000;
    RagdollLeftShin.body.mass = 1000;
    RagdollRightShin.body.mass = 1000;

    RagdollSmile.body.angle = 60;
    RagdollTorso.body.angle = 60;
    RagdollRightArm.body.angle = 60;
    RagdollLeftArm.body.angle = 60;
    RagdollRightForearm.body.angle = 60;
    RagdollLeftForearm.body.angle = 60;
    RagdollWaist.body.angle = 60;
    RagdollLeftThigh.body.angle = 60;
    RagdollRightThigh.body.angle = 60;
    RagdollLeftShin.body.angle = 60;
    RagdollRightShin.body.angle = 60;
    Skateboard.body.angle = 60;

    ramp.body.static = true;

    ramp.body.friction = 0;
    rock.body.static = true;
}
