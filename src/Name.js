
var PlayerName, chr= String.fromCharCode(65), integerchar= 0, first=true;

/**
 * Moves down to the next letter to select
 */
function moveLetterDown()
{
   integerchar--;
    chr = String.fromCharCode(65 + integerchar);
}

/**
 * Moves up to the next letter to select
 */
function moveLetterUp()
{
    integerchar++;
    chr = String.fromCharCode(65 + integerchar);

}

/**
 * Stored a selected letter.
 */
function StoreChar()
{
    chr = String.fromCharCode(65 + integerchar);
    if(first == false)PlayerName += chr;

    if(first == true)
    {
        PlayerName = chr;
        first = false;
    }



}
