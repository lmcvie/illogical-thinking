
/**
 * Loads all the assets to be used for the game
 */
function loadAssets()
{
    game.load.audio("ButtonPress", "assets/buttonpress.ogg");
    game.load.audio("BoneBreak", "assets/BoneBreak.ogg");
    game.load.audio("Bounce", "assets/bounce.ogg");
    game.load.audio("Spike", "assets/Spike.ogg");
    game.load.audio("music", "assets/TheBuilder.ogg");
    loadSprite("NewButton", "assets/NewGameButton.png");
    loadSprite("HighScoresButton", "assets/HighScoresButton.png");
    loadSprite("RagdollMenu", "assets/RagdollMenu.png");
    loadSprite("EarthMenu", "assets/EarthMenu.png");
    loadSprite("Title", "assets/title.png");
    loadSprite("Sky","assets/SkyMenu.png");
    loadSprite("Ground", "assets/EarthMenu.png");
    loadSprite("Box", "assets/Box.jpg");
    loadSprite("SubmitButton","assets/SubmitButton.png");


    loadSprite("RagdollSmile","assets/RiggingModel/RagdollSmile.png");
    loadSprite("RagdollTorso","assets/RiggingModel/RagdollTorso.png");
    loadSprite("RagdollArm","assets/RiggingModel/RagdollArm.png");
    loadSprite("RagdollLeftForearm","assets/RiggingModel/RagdollLeftForearm.png");
    loadSprite("RagdollRightForearm","assets/RiggingModel/RagdollRightForearm.png");
    loadSprite("RagdollWaist","assets/RiggingModel/RagdollWaist.png");
    loadSprite("RagdollThigh","assets/RiggingModel/RagdollThigh.png");
    loadSprite("RagdollShin","assets/RiggingModel/RagdollShin.png");

    loadSprite("earth", "assets/earth.png");
    loadSprite("tileground","assets/ground.png");
    loadSprite("TrampolineA", "assets/Trampoline.png");
    loadSprite("TrampolineB", "assets/Trampoline.png");
    loadSprite("Steelspike", "assets/Steelspike.png");
    loadSprite("sun","assets/sun.png");

    loadSprite("ScoreTile", "assets/ScoreTile.png");
    loadSprite("RagdollScores","assets/RagdollScores.png");
    loadSprite("Back", "assets/Back.png");
    loadSprite("Skateboard","assets/skateboard.png");
    loadSprite("Rock", "assets/rock.png");
    loadSprite("LowPower","assets/LowPower.png");
    loadSprite("MediumPower","assets/MediumPower.png");
    loadSprite("HighPower","assets/HighPower.png");
    loadSprite("VeryHighPower","assets/VeryHighPower.png");
    loadSprite("GuageText", "assets/guagetext.png");
    loadSprite("MoveUp", "assets/moveUp.png");
    loadSprite("MoveDown", "assets/MoveDown.png");
    loadSprite("Select", "assets/Select.png");
}


/**
 * This function just packages phasers game.load.image into an easier to remember function.
 * @param {string} name is the name that is to be assigned to the sprite for draw calls and for referring to the sprite
 * @param {number} filePath is where the file can be found.
 */
function loadSprite(name, filePath)
{
    game.load.image(name,filePath);
}

/**
 * This function allows objects or variables to equal a sprite. Can be used to manipulate the sprite.
 * @param {string} name is the name that loadSprite assigned to the sprite.
 * @param {number} xCoord is the xcoord that the sprite is to be drawn at.
 * @param {number} yCoord is the xcoord that the sprite is to be drawn at.
 * @param {number} iwidth is the total width that the sprite is to be drawn over. Can also be used for sprite sheets
 * @param {number} iheight is the total height that the sprite is to be drawn over. Can also be used for sprite sheets.
 * @returns {*} a value that an object or variable can be made to equal. This is now associated with the sprite that has been drawn and can be used to manipulate it
 */
function drawSprite(name, xCoord, yCoord, iwidth, iheight)
{
    return(this.name = game.add.sprite(xCoord, yCoord, name));
    this.name.height = iheight;
    this.name.width = iwidth;
}


/**
 *
 * @param {string} name is the name that loadSprite assigned to the sprite.
 * @param {number} xCoord is the xcoord that the sprite is to be drawn at.
 * @param {number} yCoord is the xcoord that the sprite is to be drawn at.
 * @param {number} iwidth is the total width that the sprite is to be drawn/tiled over.
 * @param {number} iheight is the total height that the sprite is to be drawn/tiled over.
 * @returns {*} a value that an object or variable can be made to equal. This is now associated with the sprite that has been drawn and can be used to manipulate it
 */
function drawTiledSprite(name, xCoord, yCoord, width, height)
{
    return(this.name = game.add.tileSprite(xCoord, yCoord, width, height, name));
}
