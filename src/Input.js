

/**
 * sets up object for mouse/touch input
 * @param {string} name is the object to be enabled for input
 */
 function setUp(name)
 {
    name.inputEnabled = true;
 }

/**
 *checks for mouse/touch input on object
 * @param {string} name is the object to be checked for mouse/touch event
 * @param {function} targetFunc is the function to be executed of the button is presed.
 */
 function inputCheck(name, targetFunc)
 {
    name.events.onInputDown.add(targetFunc, this);
 }


/**
 * increases force every click/touch.
 */
function keyCheckPowerBar()
{
   RagdollTorso.force +=40;
}